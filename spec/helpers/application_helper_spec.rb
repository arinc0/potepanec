require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    let!(:no_title) { full_title('') }
    let!(:title) { full_title('SINGLE Product') }

    it 'タイトルが正常に表示されること' do
      expect(no_title).to eq 'BIGBAG Store'
      expect(title).to eq 'SINGLE Product - BIGBAG Store'
    end
  end
end
