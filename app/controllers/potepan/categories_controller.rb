class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxons = Spree::Taxon.includes(:products)
    @taxonomies = Spree::Taxonomy.all
    option_type_of_color = Spree::OptionType.find_by(presentation: 'Color')
    @all_colors_of_products = option_type_of_color.option_values if option_type_of_color
    option_type_of_size = Spree::OptionType.find_by(presentation: 'Size')
    @all_size_of_products = option_type_of_size.option_values if option_type_of_size
    @products = params[:sort] ? sort_products(filter_products) : filter_products
    @selected = params[:sort]
  end

  def filter_by_color(color)
    products = Spree::Product.with_option_value('tshirt-color', color)
    products_filter_by_color = []
    products.each do |product|
      products_filter_by_color << product if product.taxons.pluck(:id).any? { |i| Spree::Taxon.find(params[:id]).self_and_descendants.pluck(:id).include?(i) }
    end
    @products_filter_by_color = products_filter_by_color
  end

  def filter_by_size(size)
    products = Spree::Product.with_option_value('tshirt-size', size)
    products_filter_by_size = []
    products.each do |product|
      products_filter_by_size << product if product.taxons.pluck(:id).any? { |i| Spree::Taxon.find(params[:id]).self_and_descendants.pluck(:id).include?(i) }
    end
    @products_filter_by_size = products_filter_by_size
  end

  def filter_products
    if params[:color]
      filter_by_color(params[:color])
      @products_filter_by_color
    elsif params[:size]
      filter_by_size(params[:size])
      @products_filter_by_size
    else
      Spree::Product.includes(variants_including_master: %i[images prices]).in_taxon(@taxon)
    end
  end

  def sort_products(products)
    if params[:sort] == 'NEW_PRODUCTS'
      products.sort_by(&:available_on).reverse
    elsif params[:sort] == 'LOW_PRICE'
      products.sort_by(&:price)
    elsif params[:sort] == 'HIGH_PRICE'
      products.sort_by(&:price).reverse
    elsif params[:sort] == 'OLD_PRODUCTS'
      products.sort_by(&:available_on)
    else
      products.sort_by(&:available_on).reverse
    end
  end
end
