require 'rails_helper'

RSpec.describe 'CategoriesRequet', type: :request do
  describe 'GET #show' do
    let!(:taxonomy) { create(:taxonomy, name: 'Categories') }
    let!(:taxon) { create(:taxon, name: 'Bags', taxonomy: taxonomy) }
    let!(:product) { create(:product, name: 'RUBY ON RAILS TOTE', taxons: [taxon]) }
    let!(:other_product) { create(:product, name: 'RUBY ON RAILS MUG') }
    before do
      get potepan_category_path(taxon.id)
    end

    it 'リクエストが成功していること' do
      expect(response.status).to eq 200
    end

    it 'カテゴリー名が表示されていること' do
      expect(response.body).to include 'Categories'
    end

    it '分類名が表示されていること' do
      expect(response.body).to include 'Bags'
    end

    it '関連商品が表示されていること' do
      expect(response.body).to include 'RUBY ON RAILS TOTE'
    end

    it '関連商品以外が表示されていないこと' do
      expect(response.body).to_not include 'RUBY ON RAILS MUG'
    end

    it '@taxonに正しく値が渡されていること' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@taxonomiesに正しく値が渡されていること' do
      expect(assigns(:taxonomies)).to eq [taxonomy]
    end
  end
end
