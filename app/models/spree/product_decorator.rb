Spree::Product.class_eval do
  scope :without_self, ->(product) { where.not(id: product.id) }

  def related_products
    Spree::Product.in_taxons(taxons).includes(variants_including_master: %i[images prices])
  end

  self.whitelisted_ransackable_attributes = %w[name slug description]
end
