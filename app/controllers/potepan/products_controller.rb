class Potepan::ProductsController < ApplicationController
  MAX_NUMBER_OF_DISPLAYS = 4
  MAX_NUMBER_OF_DISPLAYS_NEW_ARRIVAL = 8

  def search
    @search_word = params[:searchWord]
    searched_products = Spree::Product.ransack(name_or_description_cont: params[:searchWord])
                                      .result.includes(variants_including_master: %i[images prices])
    @products = searched_products
  end

  def show
    @product = Spree::Product.friendly_id.find(params[:id])
    @images = @product.images
    @taxon_id = @product.taxons.first.id
    @related_products = @product.related_products.without_self(@product).distinct.sample(MAX_NUMBER_OF_DISPLAYS)
  end

  def index
    @new_arrival_products = Spree::Product.includes(
      variants_including_master: %i[images prices]
    ).order(available_on: :desc).limit(MAX_NUMBER_OF_DISPLAYS_NEW_ARRIVAL)
  end
end
