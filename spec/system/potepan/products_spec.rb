require 'rails_helper'

RSpec.describe 'ProductsSystem', type: :system do
  describe 'トップページから商品詳細ページへ' do
    t = Time.current
    let!(:taxon)       { create(:taxon) }
    let!(:product)     { create(:product, available_on: t, name: 'RUBY ON RAILS TOTE', price: 0.1599e2, taxons: [taxon]) }
    let!(:old_product) { create(:product, available_on: t - (60 * 60 * 2), name: 'RUBY ON RAILS BAG') }
    let!(:new_arrival_products) { create_list(:product, 7, available_on: t - (60 * 60 * 1)) }
    before do
      visit potepan_index_path
    end

    it 'トップページに新着商品が表示されていること' do
      within('div.featuredProductsSlider') do
        expect(page).to have_content 'RUBY ON RAILS TOTE'
        expect(page).to have_content '$15.99'
      end
    end

    it '新着商品をクリックすると新着商品の詳細ページへ遷移すること' do
      click_on 'RUBY ON RAILS TOTE'
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it '新着商品の最大表示数が MAX_NUMBER_OF_DISPLAYS_NEW_ARRIVAL と一致すること' do
      expect(page).to have_selector('div.productImage', count: Potepan::ProductsController::MAX_NUMBER_OF_DISPLAYS_NEW_ARRIVAL)
      expect(page).not_to have_content 'RUBY ON RAILS BAG'
    end
  end

  describe '商品詳細ページから商品一覧ページへ' do
    let!(:taxonomy)   { create(:taxonomy, name: 'Categories') }
    let!(:taxon)    { create(:taxon, name: 'Bags', taxonomy: taxonomy) }
    let!(:product)  { create(:product, name: 'RUBY ON RAILS TOTE', taxons: [taxon]) }
    let!(:related_product) { create(:product, name: 'RUBY ON RAILS BAG', price: 0.1599e2, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 3, taxons: [taxon]) }
    let!(:other_product) { create(:product, name: 'RUBY ON RAILS MUG') }
    before do
      visit potepan_product_path(product.id)
    end

    it '関連商品をクリックすると関連商品の商品詳細ページに遷移すること' do
      expect(page).to have_content 'RUBY ON RAILS TOTE'
      expect(page).to have_content 'RUBY ON RAILS BAG'
      click_on 'RUBY ON RAILS BAG'
      expect(current_path).to eq potepan_product_path(related_product.id)
    end

    it 'Bagsカテゴリーの商品一覧ページに遷移すること' do
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    context '関連商品の数が MAX_NUMBER_OF_DISPLAYS の数より多い場合' do
      before do
        create(:product, taxons: [taxon])
      end

      it '関連商品の最大表示数が MAX_NUMBER_OF_DISPLAYS の数と一致すること' do
        within('div.productsContent') do
          expect(page).to have_content 'RUBY ON RAILS BAG'
          expect(page).to have_content '$15.99'
          expect(page).not_to have_content 'RUBY ON RAILS MUG'
          expect(page).to have_selector('div.productBox', count: Potepan::ProductsController::MAX_NUMBER_OF_DISPLAYS)
        end
      end
    end
  end
  describe '商品検索フォーム' do
    let!(:taxonomy)   { create(:taxonomy, name: 'Categories') }
    let!(:taxon)    { create(:taxon, name: 'Bags', taxonomy: taxonomy) }
    let!(:product)  { create(:product, name: 'RUBY ON RAILS TOTE', description: 'this is A', taxons: [taxon]) }
    let!(:product_1) { create(:product, name: 'RUBY ON RAILS BAG', description: 'that is %_', taxons: [taxon]) }
    before do
      visit potepan_product_path(id: product.id)
    end

    it '商品名に検索文字列を含む商品が表示されていること' do
      find_field('searchWord').set('TOTE')
      click_on('検索')
      expect(current_path).to eq potepan_products_search_path
      expect(page).to have_content 'RUBY ON RAILS TOTE'
      expect(page).not_to have_content 'RUBY ON RAILS BAG'
    end
    it '商品説明に検索文字列を含む商品が表示されていること' do
      find_field('searchWord').set('that')
      click_on('検索')
      expect(current_path).to eq potepan_products_search_path
      expect(page).to have_content 'RUBY ON RAILS BAG'
      expect(page).not_to have_content 'RUBY ON RAILS TOTE'
    end
    it '% _ などのメタキャラクタがエスケープされていること' do
      find_field('searchWord').set('%_')
      click_on('検索')
      expect(current_path).to eq potepan_products_search_path
      expect(page).to have_content 'RUBY ON RAILS BAG'
      expect(page).not_to have_content 'RUBY ON RAILS TOTE'
    end
  end
end
