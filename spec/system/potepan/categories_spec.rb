require 'rails_helper'

RSpec.describe 'CategoriesSystem', type: :system do
  describe '商品一覧ページから商品詳細ページへ' do
    let!(:taxonomy)   { create(:taxonomy, name: 'Categories') }
    let!(:taxon_1)    { create(:taxon, name: 'Bags', taxonomy: taxonomy) }
    let!(:taxon_2)    { create(:taxon, name: 'Mugs', taxonomy: taxonomy) }
    let!(:product_1)  { create(:product, name: 'RUBY ON RAILS TOTE', taxons: [taxon_1]) }
    let!(:product_2)  { create(:product, name: 'RUBY ON RAILS MUG', taxons: [taxon_2]) }
    before do
      visit potepan_category_path(taxon_1.id)
    end

    context 'product_1の場合' do
      it '商品一覧ページから商品詳細ページに遷移すること' do
        expect(page).to have_content 'RUBY ON RAILS TOTE'
        click_on 'RUBY ON RAILS TOTE'
        expect(current_path).to eq potepan_product_path(product_1.id)
      end
    end

    context 'product_2の場合' do
      it '商品一覧ページから商品詳細ページに遷移すること' do
        expect(page).to have_content 'Mugs'
        click_on 'Mugs'
        expect(page).to have_content 'RUBY ON RAILS MUG'
        click_on 'RUBY ON RAILS MUG'
        expect(current_path).to eq potepan_product_path(product_2.id)
      end
    end
  end

  describe 'カラーフィルター' do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, name: 'RUBY ON RAILS TEE', taxons: [taxon], option_types: [option_type_1]) }
    let!(:other_product) { create(:product, name: 'RUBY ON RAILS MUG', taxons: [taxon]) }
    let!(:option_type_1) { create(:option_type, presentation: 'Color', name: 'tshirt-color', option_values: [option_value_1], id: 1) }
    let!(:option_value_1) { create(:option_value, name: 'Blue', option_type_id: 1) }

    it '選択した色に関連する商品が表示されること' do
      # visit potepan_category_path(taxon.id, color: 'Blue')
      # within('div.productBox') do
      # expect(page).to have_content 'RUBY ON RAILS TEE'
      # expect(page).not_to have_content 'RUBY ON RAILS MUG'
      # end
    end
  end

  describe '選択した順番に並び替えが行われていること' do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, name: 'RUBY ON RAILS TEE', taxons: [taxon], available_on: Time.current - (60 * 60 * 1), price: 0.1599e2) }
    let!(:other_product) { create(:product, name: 'RUBY ON RAILS MUG', taxons: [taxon], available_on: Time.current - (60 * 60 * 2), price: 0.1499e2) }

    it '新着順に並んでいること' do
      visit potepan_category_path(taxon.id, sort: 'NEW_PRODUCTS')
      products = page.all('.productCaption')
      expect(products[0].find('h5').text).to eq 'RUBY ON RAILS TEE'
      expect(products[1].find('h5').text).to eq 'RUBY ON RAILS MUG'
    end
    it '古い順に並んでいること' do
      visit potepan_category_path(taxon.id, sort: 'OLD_PRODUCTS')
      products = page.all('.productCaption')
      expect(products[0].find('h5').text).to eq 'RUBY ON RAILS MUG'
      expect(products[1].find('h5').text).to eq 'RUBY ON RAILS TEE'
    end
    it '値段の高い順に並んでいること' do
      visit potepan_category_path(taxon.id, sort: 'HIGH_PRICE')
      products = page.all('.productCaption')
      expect(products[0].find('h5').text).to eq 'RUBY ON RAILS TEE'
      expect(products[1].find('h5').text).to eq 'RUBY ON RAILS MUG'
    end
    it '値段の安い順に並んでいること' do
      visit potepan_category_path(taxon.id, sort: 'LOW_PRICE')
      products = page.all('.productCaption')
      expect(products[0].find('h5').text).to eq 'RUBY ON RAILS MUG'
      expect(products[1].find('h5').text).to eq 'RUBY ON RAILS TEE'
    end
  end
end
