module Potepan::CategoriesControllerHelper
  def filter_by_color(color)
    products = Spree::Product.with_option_value('tshirt-color', color)
    products_filter_by_color = []
    products.each do |product|
      products_filter_by_color << product if product.taxons.pluck(:id).any? { |i| Spree::Taxon.find(params[:id]).self_and_descendants.pluck(:id).include?(i) }
    end
    products_filter_by_color
  end

  def filter_by_size(size)
    products = Spree::Product.with_option_value('tshirt-size', size)
    products_filter_by_size = []
    products.each do |product|
      products_filter_by_size << product if product.taxons.pluck(:id).any? { |i| Spree::Taxon.find(params[:id]).self_and_descendants.pluck(:id).include?(i) }
    end
    products_filter_by_size
  end
end
